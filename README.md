### Utilisation

- Compile
```
make artifact
```

- Run it !
```
./build/gobin
```

### Development

- Build Docker image
```
make build
```

- Run app locally
```
make start
```
