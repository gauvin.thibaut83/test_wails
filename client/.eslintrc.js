module.exports = exports = {
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module',
  },
  plugins: ['svelte3'],
  extends: ['eslint:recommended'],
  settings: {
    // There is no support for eslint sass yet
    // @see https://github.com/sveltejs/eslint-plugin-svelte3#svelte3ignore-styles
    'svelte3/ignore-styles': () => true,
  },
  overrides: [
    {
      files: ['**/*.svelte'],
      processor: 'svelte3/svelte3',
    },
  ],
};
