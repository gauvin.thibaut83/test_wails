FROM golang:1.16

WORKDIR /docker

# Check new releases here: https://nodejs.org/fr/download/releases/
ENV NODEJS_VERSION=14.16.0
ENV DOCKER_USER_ID=1001

RUN apt-get update -q \
    # Install base dependencies
    && apt-get install -yq --no-install-recommends \
        software-properties-common libgtk-3-dev libwebkit2gtk-4.0-dev \
    # Install nodejs
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get -y install nodejs \
    # Create dedicated Docker user
    && groupadd --gid ${DOCKER_USER_ID} wails \
    && useradd \
        --comment 'wails user' \
        --create-home \
        --home-dir /home/wails \
        --uid ${DOCKER_USER_ID} \
        --gid ${DOCKER_USER_ID} \
        --shell /bin/bash \
        wails \
    # Cleanup
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

USER wails

ENV GO111MODULE=on

RUN go get -u github.com/wailsapp/wails/cmd/wails \
    && mkdir -p /home/wails/.wails

COPY wails.json /home/wails/.wails/wails.json

CMD ["/go/bin/wails", "-help"]
