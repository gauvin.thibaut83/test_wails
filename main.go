package main

import (
	_ "embed"
	"fmt"

	"github.com/wailsapp/wails"
)

func hello(name string) string {
	return fmt.Sprintf("Hello %s !", name)
}

//go:embed client/public/build/bundle.js
var js string

//go:embed client/public/build/bundle.css
var css string

func main() {
	app := wails.CreateApp(&wails.AppConfig{
		Width:  1024,
		Height: 768,
		Title:  "test_wails",
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})

	app.Bind(hello)
	err := app.Run()

	if err != nil {
		return
	}
}
