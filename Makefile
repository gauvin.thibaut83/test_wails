## ----------------------
## Available make targets
## ----------------------
##

default: help

help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_\-\.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## ----------------------
## Builds
## ----------------------
##

artifact: ## Compile app from sources (linux)
	@wails build

##
## ----------------------
## Docker
## ----------------------
##

build: ## Compile app from sources (linux)
	@docker build -t test_wails:dev .
.PHONY: build

start: ## start
	@docker-compose up -d

stop: ## stop
	@docker-compose down

restart: stop start ## restart

logs: ## logs
	@docker-compose logs -f

qa: go.qa client.qa

##
## ----------------------
## Golang
## ----------------------
##

go.shell: ## go shell
	@docker-compose run --rm wails bash

go.qa: go.lint ## Run QA on go side

go.lint: ## Lint go files
	@golangci-lint run -v

##
## ----------------------
## Client
## ----------------------
##

client.shell: ## client shell
	@docker-compose run --rm client bash

client.qa: client.lint

client.lint:
	@docker-compose run --rm client /bin/sh -c ' \
		npm run eslint; \
		npm run prettier; \
	'
